from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .views import *
from .models import *
from .forms import *
from .apps import MainConfig
from django.apps import apps

class DonaturUnitTest(TestCase):
    def test_url(self):
        response = Client().get('/terimakasih/')
        self.assertEqual(response.status_code, 200) 

    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200) 

    def test_using_func(self):
        found = resolve('/formulir/')
        self.assertEqual(found.func, formulir_donatur)

#test object yang dibuat sesuai ngga sama input dari form
    def test_model_isi_payment(self):
        Payment.objects.create(Nominal = 10 , Nama = "robby" , Email = "robbynurfadillah@gmail.com")
        Payments  = Payment.objects.get(Nominal = "10")
        self.assertEqual(Payments.Nominal, 10)
        self.assertEqual(str(Payments.Nama), "robby")
        self.assertEqual(str(Payments.Email), "robbynurfadillah@gmail.com")

    def test_model_payment(self):
        Payment.objects.create(Nominal = "10" , Nama = "robby" , Email = "robbynurfadillah@gmail.com")
        counter = Payment.objects.all().count()
        self.assertEqual(counter, 1)

    def test_template(self):
        response = Client().get('/formulir/')
        self.assertTemplateUsed(response, 'donatur.html')

#ini ditambahin
    def test_get(self):
        response = Client().get('/donatur/')
        html_kembalian = response.content.decode('utf8')




    def test_get(self):
        response = Client().get('/formulir/')
        html_return = response.content.decode('utf8')
        self.assertIn("Peraturan", html_return)
        self.assertIn("Nominal", html_return)
        self.assertIn("Donatur", html_return)