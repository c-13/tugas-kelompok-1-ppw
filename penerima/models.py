from django.db import models

# Create your models here.
class Formulir(models.Model):
    namalengkap= models.CharField(max_length=60)
    email = models.EmailField()
    pekerjaan = models.CharField(max_length=60)
    kota = models.CharField(max_length=60)
    jmldana = models.IntegerField()
    ceritasingkat = models.TextField(max_length=150)
    
class Display(models.Model):
    disp= models.ForeignKey(Formulir, on_delete=models.CASCADE)