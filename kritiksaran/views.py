from django.http import HttpResponseRedirect
from django.shortcuts import render,redirect

from .forms import FormulirKritikSaran
from .models import FormKritikSaran, Tampilan
# Create your views here.

def form_kritik_saran(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FormulirKritikSaran(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            daftar= FormKritikSaran()
            daftar.namainitial=form.cleaned_data['namainitial']
            daftar.kritik=form.cleaned_data['kritik']
            daftar.saran=form.cleaned_data['saran']        
            daftar.save(True)
            # redirect to a new URL:
        return redirect('/kritik-saran') 

    # if a GET (or any other method) we'll create a blank form
    else:
        masukan= FormKritikSaran.objects.all()
        form = FormulirKritikSaran()
        response = {"masukan":masukan, 'form':form}
        return render(request, 'formkritiksaran.html',response)

def display_kritik_saran(request):
    form= FormKritikSaran.objects.all()
    response={'form':form}
    return render(request, 'displaykritiksaran.html', response)